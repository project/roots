<?php
/*
Displays a single Drupal page.

Available variables:

General utility variables:

$base_path: The base URL path of the Drupal installation. At the very least, this will always default to /.
$css: An array of CSS files for the current page.
$directory: The directory the theme is located in, e.g. themes/garland or themes/garland/minelli.
$is_front: TRUE if the current page is the front page.
$logged_in: TRUE if the user is registered and signed in.
$is_admin: TRUE if the user has permission to access administration pages.

Page metadata:

$language: (object) The language the site is being displayed in. $language->language contains its textual representation. $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
$head_title: A modified version of the page title, for use in the TITLE element.
$head: Markup for the HEAD element (including meta tags, keyword tags, and so on).
$styles: Style tags necessary to import all CSS files for the page.
$scripts: Script tags necessary to load the JavaScript files and settings for the page.
$body_classes: A set of CSS classes for the BODY tag. This contains flags indicating the current layout (multiple columns, single column), the current path, whether the user is logged in, and so on.

Site identity:

$front_page: The URL of the front page. Use this instead of $base_path, when linking to the front page. This includes the language domain or prefix.
$logo: The path to the logo image, as defined in theme configuration.
$site_name: The name of the site, empty when display has been disabled in theme settings.
$site_slogan: The slogan of the site, empty when display has been disabled in theme settings.
$mission: The text of the site mission, empty when display has been disabled in theme settings.

Navigation:

$search_box: HTML to display the search box, empty if search has been disabled.
$primary_links (array): An array containing primary navigation links for the site, if they have been configured.
$secondary_links (array): An array containing secondary navigation links for the site, if they have been configured.

Page content (in order of occurrence in the default page.tpl.php):

$breadcrumb: The breadcrumb trail for the current page.
$title: The page title, for use in the actual HTML content.
$help: Dynamic help text, mostly for admin pages.
$messages: HTML for status and error messages. Should be displayed prominently.
$tabs: Tabs linking to any sub-pages beneath the current page (e.g., the view and edit tabs when displaying a node).
$content: The main content of the current Drupal page.
$node: The node object, if there is an automatically-loaded node associated with the page, and the node ID is the second argument in the page's path (e.g. node/12345 and node/12345/revisions, but not comment/reply/12345).

Footer/closing data:

$feed_icons: A string of all feed icons for the current page.
$footer_message: The footer message as defined in the admin settings.
$closure: Final closing markup from any modules that have altered the page. This variable should always be output last, after all other dynamic content.

Block regions:

$region_header : The top region.
$region_page_left: The HTML for the left sidebar.
$region_page_top: The HTML for the content (above).
$region_page_bottom: The HTML for the content (below).
$region_page_right: The HTML for the right sidebar.
$region_footer : The footer region.

Roots variables:

$attr: Body attributes.
$skipnav: Link that teleports the user to the content.
$is_prose: Flag for filtered content.
$pre_object : Content placed before the node.
$post_object : Content placed after the node.
$tabs1: Tabbed menu level one.
$tabs2: Tabbed menu level two.

*/

$output = '<!DOCTYPE html>
<html lang="'. $language->language.'" dir="'. $language->dir.'">
  <head>
    <!--[if IE]><![endif]-->    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">

    <title>'. $head_title .'</title>

    '. $head.'
    '. $styles.'
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans&amp;subset=latin" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Nobile" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" href="/favicon.ico">
    <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
  </head>
  <body '. drupal_attributes($attr) .'>
    '. $skipnav .' 
    <div id="wrapper">
      '. ($region_header ? '<div id="header"><div class="limiter clear">'. $region_header .'</div></div>' : '') .'
      <header>
        <div class="limiter clear">
          <h1 class="site-name">'. $site_name.'</h1>
          '. ($search_box ? '<div class="block block-theme">'. $search_box .'</div>' : '') .'
        </div>
      </header>
      <nav id="navigation">
        <div class="limiter clear">
          '. ((isset($primary_links)) ? theme('links', $primary_links, array('class' => 'links primary-links')) : '') .'
          '. ((isset($secondary_links)) ? theme('links', $secondary_links, array('class' => 'links secondary_links')) : '') .'
        </div>
      </nav>
      <div id="console">
        <div class="limiter clear">
          '. $help .'
          '. (($show_messages && $messages) ? $messages : '') .'
        </div>
      </div>
      <section id="page">
        <div class="limiter clear">
          '. ($region_page_left ? '<div id="page_left" class="clear">'. $region_page_left.'</div>' : '') .'
          <div id="page_main" class="clear">
            '. ($region_page_top ? '<div id="page_top" class="clear">'. $region_page_top.'</div>' : '') .'
            '. ($mission ? '<div id="mission">'. $mission .'</div>' : '') .'
            '. ($title ? '<h1 class="page-title">'. $title.'</h1>' : '') .'
            '. ($tabs1 ? $tabs1 : '') .'
            '. ($tabs2 ? $tabs2 : '') .'
            <div id="content" class="clear">
              '. $content.'
            </div>
            '. ($region_page_bottom ? '<div id="page_bottom" class="clear">'. $region_page_bottom.'</div>' : '') .'
          </div>
          '. ($region_page_right ? '<div id="page_right" class="clear">'. $region_page_right.'</div>' : '') .'
        </div>
      </section>
      <footer id="footer">
        <div class="limiter clear">
          '. ($breadcrumb ? $breadcrumb : '') .'
          '. $feed_icons.'
          '. $region_footer.'
          '. $footer_message.'
        </div>
      </footer>
    </div>
    '. $scripts.'
    <!--[if lt IE 7 ]>
      <script type="text/javascript" src="http://dillerdesign.com/experiment/DD_belatedPNG/DD_belatedPNG_0.0.8a-min.js"></script>
      <script type="text/javascript">
			DD_belatedPNG.fix(".icon");
      </script>
    <![endif]-->
   '. $closure.'
  </body>
</html>';

print $output;
?>