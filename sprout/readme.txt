Sprout - an overview
-------------------
Sprout is a sub-theme to roots. Sprout is where all the customizing happens.

The main features are:
* reasonable backend style
* some (minimal) inspiration for your individual frontend style

What's next? Sprout is a work in progress. It is meant as a starting
point - so go ahead and enjoy. 

