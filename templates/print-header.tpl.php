<?php
/*
Theme implementation to display a header for print layout.

Roots variables:

$site_name: Website title.
$theme_path: Path to the theme.

*/

$output = '
<div class="print-header">'."\n";
if (file_exists("{$theme_path}/print/logo.png")) {
  $output .= '<img class="logo" alt="'. $site_name .'" src="'. $theme_path .'/print/logo.png" />'."\n";
}
else {
  $output .= '<h1 class="site-name">'. $site_name .'</h1>'."\n";
}
$output .= '</div>'."\n";

print $output;
?>