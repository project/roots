<?php
/*
Theme implementation to display a form item.

Roots variables:

$attr: wrapper attributes
$label_title: form item label.
$label_attr: label attributes

$value: Form item content.
$description: Form item description (help text).

*/

$output = '
<div '. ((!empty($attr)) ? drupal_attributes($attr) : '') .'>'."\n";
if (!empty($label_title)) {
  $output .= '<label '. ((!empty($label_attr)) ? drupal_attributes($label_attr) : '') .'>'. $label_title .'</label>'."\n";
}
if (!empty($value)) {
  $output .= $value;
}
if (!empty($description)) {
  $output .= '  <div class="description">'. $description .'</div>'."\n";
}
$output .= '</div>'."\n";

print $output;
?>