<?php
/*
Theme implementation to display a fieldset.

Roots variables:

$attr: Wrapper attributes.
$hook: Hook name.
$title: Fieldset legend.
$content: Fieldset content.
$is_prose: Flag for filtered content.

*/

$output = '
<fieldset '. ((!empty($attr)) ? drupal_attributes($attr) : '') .'>
  <legend>
    <span class="'. $hook .'-title">'. $title .'</span>
  </legend>
  <div class="'. $hook .'-content clear '. ((!empty($is_prose)) ? 'prose' : '') .'">
    '. $content .'
  </div>
</fieldset>'."\n";

print $output;
?>