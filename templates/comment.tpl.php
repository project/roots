<?php
/*
Theme implementation to display a comment.

Available variables:

$author: Comment author. Can be link or plain text.
$content: Body of the post.
$date: Date and time of posting.
$links: Various operational links.
$new: New comment marker.
$picture: Authors picture.
$signature: Authors signature.
$status: Comment status. Possible values are: comment-unpublished, comment-published or comment-preview.
$submitted: By line with date and time.
$title: Linked title.

These two variables are provided for context.

$comment: Full comment object.
$node: Node object the comments are attached to.

Roots variables:

$attr: Wrapper attributes.
$hook: Hook name.
$is_prose: Flag for filtered content.

*/

$output = '
<div '. ((!empty($attr)) ? drupal_attributes($attr) : '') .'>
  '. ((!empty($title)) ? '<h2 class="'. $hook .'-title">'. $title .'</h2>' : '') .'
  '. ((!empty($submitted)) ? '<div class="'. $hook .'-submitted clear">'. $submitted .'</div>' : '') .'
  <div class="'. $hook .'-content clear '. ((!empty($is_prose)) ? 'prose' : '') .'">
    '. $content .'
  </div>
  '. ((!empty($links)) ? '<div class="'. $hook .'-links clear">'. $links .'</div>' : '') .'
</div>'."\n";

print $output;
?>