<?php
/*
Theme implementation to display a box.

Available variables:

$title: Box title.
$content: Box content.

Roots variables:

$attr: Wrapper attributes.
$hook: Hook name.

*/

$output = '
<div '. ((!empty($attr)) ? drupal_attributes($attr) : '') .'>
  '. ((!empty($title)) ? '<h2 class="'. $hook .'-title">'. $title .'</h2>' : '') .'
  <div class="'. $hook .'-content clear">
    '. $content .'
  </div>
</div>'."\n";

print $output;
?>