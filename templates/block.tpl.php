<?php
/*
Theme implementation to display a block.

Available variables:

$title: Block title.
$content: Block content.
$block: Full block object.
$block->module: Module that generated the block.
$block->delta: This is a numeric id connected to each module.
$block->region: The block region embedding the current block.

Helper variables:

$block_zebra: Outputs 'odd' and 'even' dependent on each block region.
$zebra: Same output as $block_zebra but independent of any block region.
$block_id: Counter dependent on each block region.
$id: Same output as $block_id but independent of any block region.
$is_front: Flags true when presented in the front page.
$logged_in: Flags true when the current user is a logged-in member.
$is_admin: Flags true when the current user is an administrator.

Roots variables:

$attr: Wrapper attributes.
$hook: Hook name.
$is_prose: Flag for filtered content.

*/

$output = '
<div '. ((!empty($attr)) ? drupal_attributes($attr) : '') .'>
  '. ((!empty($title)) ? '<h2 class="'. $hook .'-title">'. $title .'</h2>' : '') .'
  <div class="'. $hook .'-content clear '. ((!empty($is_prose)) ? 'prose' : '') .'">
    '. $content .'
  </div>
</div>'."\n";

print $output;
?>