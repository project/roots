Roots - an overview
-------------------
Roots theme takes you back to the roots. It's a modern HTML5 + CSS3 theme blueprint 
tailored to fit drupal. It is inspired by drupal `HTML5 boilerplate` (Paul Irish and 
Divya Manian) and drupal theme 'tao'.

The main features are:
* browser default style reset
* complete drupal core style reset
* server side css browser selection - no more hacks or conditional markup
* implementation of a meaningfull typographic base grid / style 
* css expert skill included (kudos boilerplate)
* theme preprocessors (kudos tao theme)
* custom theme settings for design development

What's next? Just implement your design as a sub-theme to roots to benfit from a lean 
and clean base. As a starting point for rapid development the sub-theme `Sprout` is in 
the package. Sprout provides basically a clean admin theme and some ideas about
how to build up on roots.


Under the hood
--------------

## Style reset

The roots style reset is a HTML5 reset (including deprecated html tags).
To overcome the annoying work of re-styling drupal core elements (eg. ul.menu)
the roots base theme excludes all drupal core style sheets. Saved bandwidth is a 
free addon.

Some functional drupal styles (auto-complete, draggables ..) are re-implemented via
the `roots.drupal` stylesheet in a way that does not affect the overall clean look.
Please note that all addon modules are free to add their own styles.

As we all don't like ugly css hacks or browser specific style-sheet, a server side 
css browser selection is included. You can target almost any browser type or feature 
by just using the classes automatically added to the body tag.

All stylesheet inclusion is done media-specific - never use media=all. It makes
your layout a lot more predictable.

For the screen media the following stylesheets are included:

* roots.reset - the bwroser default reset
* roots.base - basic definitions for readability and usability
* roots.grid - just a raw layout 
* roots.type - the typographic rythm
* roots.table - base styles for tables
* roots.form - base styles for form elements 
* roots.drupal - essential drupal core styles

For the print media we define the following stylesheets:

* roots.reset - the bwroser default reset
* roots.base - basic definitions for readability and usability
* roots.print - all the print specific stuff


## Typographic base style

So many design blueprints take care of your layout grid while there's so much 
need for vertical rythm and typographic sensibility. Roots adds this while providing 
full freedom of choice to the designer via the sub theme.

The templates will add the class "prose" to areas containing output rendered 
by drupal filters. Only those areas benefit from the typographical base style while
all other content is left quite untouched (as is the page layout).


## Alterations to core markup

Roots leaves most core element markup alone - but implements some very good 
overrides from tao theme:

* theme('fieldset')  
The `fieldset` element in core has been retained but with additional markup
for simpler theming. The `legend` element contains a sub-element `span` that
can be positioned properly across browsers and the main contents of the
fieldset follow other object templates with a `.fieldset-content` div.

* theme('form_element')  
To distinguish between different instances of the `.form-item` wrapper, Tao
adds a `.form-item-labeled` class to labeled items and `.form-item-option` to
checkboxes and radios.

* theme('pager')  
All numeric page links are grouped together into a `ul.pager-list` set.
Next/previous links are grouped together under `ul.pager-links`.

* theme('username')  
All username instances are wrapped in an `a.username` or `span.username`.


## Print style preview

Roots allows a site's print stylesheets to be previewed by checking whether
`$_GET['print']` is set. For example, to preview a node's print stylesheets, you
would go to `http://example.com/node/1?print`. 


## Templates

All template files are located in the `templates` sub-folder. They're designed to 
follow a strict pattern. Each element is classed as such: 
	`[theme_hook]-[element_type]` 
with the name of its hook and then the type of wrapping element within the template. 


## Theme settings

In the theme settings for roots you can toggle settings that provide visual aid
while growing your very own theme based on roots.



