<?php
/**
 * Implementation of theme_settings() hook.
 *
 * @param $saved_settings
 *   array An array of saved settings for this theme.
 * @return
 *   array A form array.
 */
function roots_settings($saved_settings) {
   
  // default values
  $defaults = array(
    'roots_grid_v' => 0,
    'roots_grid_h' => 0
  );

  $settings = array_merge($defaults, $saved_settings);

  $form['roots_grid_v'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display vertical grid (typo baseline)'),
    '#default_value' => $settings['roots_grid_v'],
  );

  $form['roots_grid_h'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display horizontal grid (layout raster)'),
    '#default_value' => $settings['roots_grid_h'],
  );

  return $form;
}
?>